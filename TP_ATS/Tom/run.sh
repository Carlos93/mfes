#/bin/bash
EXAMPLE_DIR=exemplos/
EXAMPLE_FILE=maiorDeDoisNumeros.i
SECOND_FILE=BadSmellNumbers.txt
RES_FILE=res.msp

export TOM_HOME=${HOME}/tom
export PATH=${PATH}:/tom/bin
export CLASSPATH=/tom/lib/tom-runtime-full.jar:${CLASSPATH}

make all
cp $EXAMPLE_DIR$EXAMPLE_FILE genI
cp $EXAMPLE_DIR$SECOND_FILE genI
cd genI
javac gram/Main.java
java gram/Main -rt < $EXAMPLE_FILE > $RES_FILE