void main() {
	int num;
	num = input(int);
	int resI;
	int resR;
	resI = fatorialI(num);
	resR = fatorialR(num);
	print(resI);
	print(resR);
}

int fatorialI(int num1){
	int fat;
	for(fat = 1; num1 > 1; num1--){
		fat = fat * num1;
	}
	return fat;
}

int fatorialR(int num2){
	if (num2==1 || num2==0){
		return 1;
	}
	else{
		return num2*fatorialR(num2-1);
	}
}